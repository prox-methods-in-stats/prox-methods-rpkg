#'
#' This file contains some helper functions for BayesMAP.
#' Specifically, knitr labeled loss, penalty and prox functions
#' and whatever else might show up more than once across all our
#' examples.
#'
#' -bwillard
#'

#'
#' This method just knits example files in the project root's output/ directory.
#' Since there was some difficulty getting this to work via knitr and rmarkdown
#' alone, this function copies files into output, then compiles them.
#'
#' @param file.prefix the name of the file without directory or extension
#' @param open.html open the html file in a browser after compiling
#' @param use.local.envir run the code in the calling environment
#'
knit.example = function(file.prefix, 
                        open.html=FALSE, use.local.envir=FALSE,
                        output.dir = "../../../output") {
  #library(devtools)
  #install_github("rstudio/rmarkdown")
  require(rmarkdown)
  require(knitr)

  out.dir = normalizePath(output.dir)
  file.name = paste0(file.prefix,'.rmd')

  this.file = file.path(out.dir,file.name)
  out.md.file = file.path(out.dir,paste0(file.prefix,'.md'))
  out.html.file = file.path(out.dir,paste0(file.prefix,'.html'))

  opts_chunk$set(fig.path="figure/")
  opts_knit$set(output.dir=out.dir)
  opts_knit$set(root.dir=out.dir)

  #
  # copy this file to the output directory, since
  # knitr needs the figures to be relative to the src file's dir.
  #
  stopifnot(file.copy(file.name, out.dir, overwrite=TRUE))
  stopifnot(file.copy("include", out.dir, recursive=TRUE))
  stopifnot(file.copy("bayes-map-tools.r", out.dir, overwrite=TRUE))
  stopifnot(file.copy("_output.yaml", out.dir, overwrite=TRUE))

  this.envir=environment()
  if (use.local.envir)
    this.envir=parent.env(this.envir)

  # creates html file
  rmarkdown::render(this.file,  
                    output_dir=out.dir, 
                    intermediates_dir=out.dir,
                    clean=TRUE,
                    envir=this.envir)

  # open in a browser
  if (open.html)
    browseURL(paste0('file://', out.html.file)) 
}

## @knitr noprox
no.prox = function(x, ...) x

mm.logit.xi = function(x, env) {
  xi.res = crossprod(env$A, env$m * plogis(env$A %*% x) - env$y)
  return(xi.res)
}

## @knitr logitloss
logit.loss.fun = function(x, env) {
  Ab = env$A %*% x
  return(sum(env$m * log(1+exp(Ab))) - crossprod(env$y, Ab))
  #return(sum(env$m*log(cosh(env$A %*% x/2))-env$kappa * env$A %*% x - log(2)))
}

## @knitr logitparetoobj
logit.pareto.obj.fun = function(x, env) {
  return(logit.loss.fun(x,env) + env$lambda * sum(log(1+abs(x)/env$q)))
}

## @knitr logitl1obj
logit.l1.obj.fun = function(x, env) {
  return(logit.loss.fun(x, env) + env$lambda * sum(abs(env$B %*% x)))
}

## @knitr logitparetoobjgrad
logit.pareto.obj.fun.grad = function(x, env) {
  return(diag(env$A)*sum(-env$kappa + 0.5 * env$m * tanh(env$A %*% x)) 
             - env$lambda/(env$q-x))
}

## @knitr logitlossgrad
logit.loss.fun.grad = function(x, env) {
  #x.grad = t(env$A) %*% (env$m * plogis(env$A %*% x) - env$kappa)
  x.grad = t(env$A) %*% (env$m * plogis(env$A %*% x) - env$y)
  return(x.grad)
}

## @knitr l2loss
l2.loss.fun = function(x, env) {
  return(sum(crossprod(env$A %*% x - env$y)/2))
}

## @knitr l2lossgrad
l2.loss.fun.grad = function(x, env) {
  AtA = mget("AtA", env=list2env(env), mode="numeric", 
            list(function(x) crossprod(env$A)))[[1]]
  Aty = mget("Aty", env=list2env(env), mode="numeric", 
                   list(function(x) crossprod(env$A,env$y)))[[1]]
  AtAx = AtA %*% x
  return(AtAx - Aty)
}

## @knitr softprox
soft.prox = function(x, lambda, ...) {
  return(sign(x) * pmax(abs(x)-lambda, 0))
}

## @knitr paretoprox
double.pareto.prox = function(x, lambda, env) {
  d = pmax(env$q * abs(x) - lambda, 0)
  return(sign(x)/2 * (abs(x) - env$q + sqrt((env$q - abs(x))^2 + 4*d)))
}

## @knitr noSeqUpdate
#'
#' Do-nothing/pass-thru step function to be used as seq.update.fun
#' in prox.grad.
#'
#' @param x current location
#'
no.seq.update = function(x, ...) x

## @knitr nestStep
#'
#' Nesterov sequence function to be used as seq.update.fun
#' in prox.grad.
#'
#' @param x current location
#' @param fk iteration number
#' @param fx.last previous location
#'
nest.seq.update = function(x, k, x.last, ...) {
  return(x + (k/(k+3))*(x - x.last))
}

## @knitr lineSearch
#'
#' Rough implementation of an Armijo line-search to be used as a step.fun
#' in prox.grad.
#'
#' @param x.last current location
#' @param x.grad gradient step 
#' @param fprox.fun proximal operator
#' @param loss.fun the problem loss function
#' @param env problem environment.  must contain a beta discount factor.
#'
line.search.step = function(x.last, x.grad, fprox.fun, loss.fun, env) {
  falpha = env$alpha
  alpha.run = falpha
  z = NULL
  repeat {
    x.step = x.last - alpha.run * x.grad
    z = fprox.fun(x.step, alpha.run * env$lambda, env) 

    if(loss.fun(z, env) <= 
         loss.fun(x.last, env) + crossprod(x.grad, z - x.last) 
           + crossprod(z - x.last)/(2*alpha.run))
      break
    alpha.run = env$beta * alpha.run
  } 
  env$alpha = alpha.run
  return(list('z' = z))
}

## @knitr pdfp2o
#'
#' PDFP2O, from Chen, composite operator proximal steps (4.12).
#' Must have a B matrix and its bound, lambda.B, in the
#' environment.
#' Running step sequence vk will be created in the environment
#' if one doesn't exist.
#'
#' @param x.last current location
#' @param x.grad gradient step 
#' @param fprox.fun proximal operator
#' @param loss.fun the problem loss function
#' @param env problem environment.  
#'
pdfp2o.step = function(x.last, x.grad, fprox.fun, loss.fun, env) {
               
  falpha = env$alpha
  vk = mget("vk", env=list2env(env), mode="numeric", 
            list(function(x) x.last ))[[1]] 
  #x.half.step = x.last - falpha * x.grad - env$lambda.B * crossprod(env$B, vk)
  #Bxpvk = env$B %*% x.half.step + vk 
  #vkp1 = Bxpvk - fprox.fun(Bxpvk, falpha/env$lambda.B * env$lambda)  
  #z = x.last - falpha * x.grad - env$lambda.B * crossprod(env$B, vkp1)
  #env$vk = vkp1
 
  x.step = x.last - falpha * x.grad
  x.half.step = fprox.fun(x.step, falpha * env$lambda, env) 
  BxpIBBvk = env$B %*% x.half.step + vk - env$lambda.B * tcrossprod(env$B) %*% vk 
  v.td.k1 = BxpIBBvk - fprox.fun(BxpIBBvk, falpha/env$lambda.B * env$lambda)  
  x.td.k1 = x.half.step - env$lambda.B * crossprod(env$B, v.td.k1)
  vk = env$po.kappa * vk + (1-env$po.kappa) * v.td.k1
  z = env$po.kappa * x.last + (1-env$po.kappa) * x.td.k1
  env$vk = vk

  return(list('z' = z))
}

## @knitr pdfp2oLine2
#'
#' PDFP2O, from Chen, composite operator proximal step 
#' (with line search gradient step) .
#' Must have B matrix and its bound lambda.B in the environment,
#' along with a picard-opial constant kappa.
#' A running step sequence vk will be created in the environment
#' if one doesn't exist.
#'
#' This version does a PDFP2O iteration within a back-tracking loop.
#'
#' @param x.last current location
#' @param x.grad gradient step 
#' @param fprox.fun proximal operator
#' @param loss.fun the problem loss function
#' @param env problem environment.  must contain a beta discount factor
#'
pdfp2o.line.step.2 = function(x.last, x.grad, fprox.fun, loss.fun, env) {

  falpha = env$alpha
  alpha.run = falpha
  vk = mget("vk", env=list2env(env), mode="numeric", 
            list(function(x) x.last ))[[1]] 
  z = NULL
  repeat {
    x.half.step = x.last - alpha.run * x.grad

    BxpIBBvk = env$B %*% x.half.step + vk - env$lambda.B * tcrossprod(env$B) %*% vk 

    v.td.k1 = BxpIBBvk - fprox.fun(BxpIBBvk, alpha.run/env$lambda.B * env$lambda)  
    x.td.k1 = x.half.step - env$lambda.B * crossprod(env$B, v.td.k1)

    vk = env$po.kappa * vk + (1-env$po.kappa) * v.td.k1
    z = env$po.kappa * x.last + (1-env$po.kappa) * x.td.k1

    if(loss.fun(z, env) <= 
         loss.fun(x.last, env) + crossprod(x.grad, z - x.last) 
           + crossprod(z - x.last)/(2*alpha.run))
      break
    alpha.run = env$beta * alpha.run
  } 
  env$vk = vk
  env$alpha = alpha.run
  return(list('z' = z))

}

## @knitr pdfp2oLine
#'
#' PDFP2O, from Chen, composite operator proximal step 
#' (with line search gradient step) .
#' Must have B matrix and its bound lambda.B in the environment,
#' along with a picard-opial constant kappa.
#' A running step sequence vk will be created in the environment
#' if one doesn't exist.
#'
#' @param x.last current location
#' @param x.grad gradient step 
#' @param fprox.fun proximal operator
#' @param loss.fun the problem loss function
#' @param env problem environment.  must contain a beta discount factor
#'
pdfp2o.line.step = function(x.last, x.grad, fprox.fun, loss.fun, env) {

  ls.step.res = line.search.step(x.last, x.grad, fprox.fun, loss.fun, env)
  falpha = env$alpha
  x.half.step = ls.step.res$z
  vk = mget("vk", env=list2env(env), mode="numeric", 
            list(function(x) x.last ))[[1]] 
  BxpIBBvk = env$B %*% x.half.step + vk - env$lambda.B * tcrossprod(env$B) %*% vk 
  v.td.k1 = BxpIBBvk - fprox.fun(BxpIBBvk, falpha/env$lambda.B * env$lambda)  
  x.td.k1 = x.half.step - env$lambda.B * crossprod(env$B, v.td.k1)
  vk = env$po.kappa * vk + (1-env$po.kappa) * v.td.k1
  z = env$po.kappa * x.last + (1-env$po.kappa) * x.td.k1
  env$vk = vk

  return(list('z' = z))
}

## @knitr constStepUpdate
#'
#' Constant step-size gradient step to be used as a step.fun
#' in prox.grad.
#' @param x.last current location
#' @param x.grad gradient step direction
#' @param env problem environment
#'
const.step.update = function(x.last, x.grad, fprox.fun, floss.fun, env) {
  falpha = env$alpha
  return(list('z' = fprox.fun(x.last - falpha * x.grad, falpha * env$lambda, env)))
}

## @knitr proxGrad
#'
#'
#' This is a generic optimization loop that uses all the passed functions
#' during it's evaluation.  It's meant to be plug-and-play for a variety
#' of Prox Gradient-like algorithms.
#' @param algo a list containing the following items: 
#'  xi.fun, prox.fun, step.fun, obj.fun, loss.fun, desc.  Where
#'  xi.fun is the gradient/descent step/direction function.
#'  step.fun is a function that determines the step size before the prox, e.g. line-search.
#'  seq.update.fun is a function that determines the step size after the prox, e.g. Nesterov.
#'  desc is a string description of the algorithm being implemented.
#' @param env other parameters that get passed to all internal functions.  this must contain the
#'  following elements:
#'  M number of covariates 
#'  N number of observations 
#'  A covariate matrix 
#'  y observations 
#'  lambda size for prox step
#'  alpha size for xi step
#'  beta line search discount factor 
#' @param max.iters run up to this many iterates (strict)
#' @param all.iters run all max.iters
#' @param propagate propagate last values to max.iters
#'
prox.grad = function(algo,
                     x0 = matrix(0, algo$env$N, 1),
                     max.iters=5000, 
                     all.iters=FALSE,
                     propagate=FALSE,
                     show.iters=FALSE) {
  x.run = x0
  x.last = x.run 
  y.last = rep(Inf, algo$env$N) 
  objval.last = Inf
  objval.diff = Inf
  iters = 0
  pg.objEval = c()
  pg.sse = c()
  if(show.iters)
    cat(sprintf('%3s\t%10s\t%10s\n', 'iter', 'sse', 'objective'))
  while(iters < max.iters && (all.iters || objval.diff > 1e-5)) {
    k = iters + 1
    y.last = algo$seq.update.fun(x.run, k, x.last, algo$env) 
    y.grad = algo$xi.fun(y.last, algo$env)

    step.res = algo$step.fun(y.last, y.grad, algo$prox.fun, algo$loss.fun, algo$env)

    x.last = x.run
    x.run = step.res$z

    objval.now = algo$obj.fun(x.run, algo$env)
    objval.diff = abs(objval.now - objval.last)
    objval.last = objval.now 

    pg.objEval = c(pg.objEval, objval.last) 
    sse.last = crossprod(algo$env$x.true - x.run)
    pg.sse = c(pg.sse, sse.last) 
    if(show.iters)
      cat(sprintf('%3d\t%10.4f\t%10.4f\n', iters, sse.last, objval.last))
    iters = iters+1
  }
  if (propagate && iters < max.iters) {
    pg.objEval = c(pg.objEval, rep(tail(pg.objEval,1), max.iters-length(pg.objEval)))
    pg.sse = c(pg.sse, rep(tail(pg.sse,1), max.iters-length(pg.sse)))

  }
  return(list(x.last = x.run,
              obj.vals = pg.objEval, 
              sse.vals = pg.sse, 
              type=algo$desc))
}

