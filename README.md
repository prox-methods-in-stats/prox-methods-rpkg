---
output:
  md_document:
    variant: markdown_github
---

<!-- this file is inspired by https://raw.githubusercontent.com/hadley/dplyr/master/README.Rmd -->
<!-- README.md is generated from README.Rmd. Please edit that file -->



# prox-methods

## Installation from R

Currently, remote installation in R (without cloning this repo and building locally) requires 
the [devtools][dt] package and something like the following:
```
library(devtools)
install_url("https://bitbucket.org/prox-methods-in-stats/prox-methods-rpkg/get/HEAD.zip", subdir="pkg")
```
then, as usual, add ```library(ProxMethods)``` to your R source to use.  
Inclusion of this file in the repo is temporary, that is, until this package
gets submitted to CRAN (upon completion of the paper and whatnot).

## Development Setup and Installation

The following is a quick, simple example of local development for this package in R
(see [here][dev] for more details).
Assuming you've pulled the source from the repository into `~/projects/code/prox-methods-rpkg`:

```R
library(devtools)

dev_mode(on=T)

#
#  Set the working directory to this project
#
proj.root = normalizePath("~/projects/code/prox-methods-rpkg")
setwd(proj.root)

install_local(proj.root)

#
#  Load all functions and variables into the environment, even
#  those that aren't expected to be exposed in public version of the package.
#
load_all("./", export_all=T)

#
#  Do some work, could be in this file, or another.
#
...

#
#  Check that your changes don't break anything else (and that they
#  work, since one should always write tests for their new code)
#
test("./")

#
#  Run these when you're happy with your changes and want to add
#  any new functions and/or update documentation.
#
document("./")

dev_mode(on=F)

```

[dev]:http://adv-r.had.co.nz/Package-development-cycle.html
[dt]:https://github.com/hadley/devtools

