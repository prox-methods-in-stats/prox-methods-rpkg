/**
 * Composite proximal methods.  Specifically, the Picard-Opial 
 * by Argyriou.
 *
 */

#include "prox-methods.h"
#include <progress.hpp> 

//'
//' Picard-Opial composite optimization (Argryiou)
//' for a a multinomial logit + l1 composite problem.  
//' Uses the Lipschitz quadratic bound.
//'
//' @param r_y observations
//' @param r_m counts
//' @param r_A covariate matrix
//' @param r_B penalty constraint matrix
//' @param r_beta0 starting point
//' @param r_lambda penalty weight
//' @param r_kappa Opial constant (between [0,1])
//' @param r_L Manually specified Lipschitz constant
//' @param r_max_iters maximum iterations
//' @param r_max_po_iters maximum Picard-Opial internal iterations
//' @param r_accel enable/disable acceleration (not implemented here)
//' @param r_throw_at_max_po stop execution if internal iterations are reached
//'   before convergence.
//'
// [[Rcpp::export("logit.l1.comp.prox")]]
List logit_l1_opial_prox_run(NumericVector r_y, 
    NumericVector r_m, NumericMatrix r_A, 
    NumericMatrix r_B, NumericVector r_beta0,
    double r_lambda, double r_kappa, double r_L = NA_REAL,
    int r_max_iters = 100, int r_max_po_iters = 10000, 
    bool r_accel = true, bool r_throw_at_max_po = false) { 

  const arma::vec y = Rcpp::as<arma::vec>(r_y);
  const arma::vec m = Rcpp::as<arma::vec>(r_m);
  const arma::mat B = Rcpp::as<arma::mat>(r_B);
  const arma::mat A = Rcpp::as<arma::mat>(r_A);

  // Opial k-average/contraction constant?
  const double kappa = r_kappa; //as<double>(r_kappa);
  // regularization weight/factor
  const double lambda = r_lambda; //as<double>(r_lambda);

  const int N = B.n_rows;
  const int M = B.n_cols;

  const double omega_A = pow(arma::norm(A, 2), 2);
  double L;
  if (!R_IsNA(r_L))
    L = r_L;
  else
    L = omega_A/4.0;

  double omega_B;
  double omega;
  if(N > M) {
    omega_B = pow(arma::norm(B, 2), 2);
    omega = 2.0 * L / omega_B;
  } else {
    // eigen value bound of constraint matrix B
    mat U_B,V_B;
    vec s_vals_B;
    // truncate at small values
    s_vals_B.transform([] (double s) {
          return (fabs(s) < 1e-7) ? 0.0 : s;
        });
    arma::svd(U_B, s_vals_B, V_B, B);
    omega_B = pow(arma::max(s_vals_B), 2);
    omega = 2.0 * L/ (omega_B + pow(arma::min(s_vals_B), 2));
  }  

  const int iters_fp = r_max_po_iters;
  const bool throw_at_max_po = r_throw_at_max_po;

  printf("N=%i, M=%i, lambda=%f, omega_B=%f, kappa=%f, max_iters=%i, accel=%i, L=%f\n", 
         N, M, lambda, omega_B, kappa, r_max_iters, r_accel, L);

  vec beta = Rcpp::as<arma::vec>(r_beta0);
  vec alpha = Rcpp::as<arma::vec>(r_beta0);// zeros(M);

  std::vector<double> obj_vals;

  // acceleration "averaging" parameter starting value
  double theta = 1.0;

  int k = 0;
  int j = 0;
  vec beta_prev = beta;
  double obj_prev = logit_l1B_obj_fun(y, A, m, B, beta, lambda);
  obj_vals.push_back(obj_prev);
  double obj_diff = 0.0; 
  Progress p(0, false);
  do {
    if (Progress::check_abort())
      return R_NilValue;
    const vec gradalpha = logit_loss_grad_fun(y, A, m, beta);
    const vec Bga = B * (gradalpha - L * alpha);
    vec v = zeros(M);
    vec prev_fp;
    int s = 0;
    double v_err = 0.0; 
    do {
      prev_fp = v;
      vec Bv = B.t() * v;
      vec Av = v - omega/L * B * Bv - Bga/L;
      v = kappa * v + (1.0 - kappa) * (Av - soft_thresh(Av, lambda, omega));
      v_err = norm(prev_fp - v, 2);
      s++;
      if (s >= iters_fp) {
        if (throw_at_max_po)
          throw Rcpp::exception("picard-opial hasn't converged under max iterations");
        printf("reached max internal fixed point iterations! v_err=%f\n", v_err);
        break;
      } 
    } while(v_err > EPS);

    j += s;

    beta_prev = beta;
    beta = -(gradalpha - L * alpha + omega * B.t() * v)/L;
    if (r_accel) {
      double theta2 = std::pow(theta, 2);
      theta = (std::sqrt(std::pow(theta2, 2) + 4.0 * theta2) - theta2)/2.0;
      const double rho = 1.0 - theta + std::sqrt(1.0-theta);
      alpha = rho * beta - (rho - 1.0) * beta_prev;
    } else {
      alpha = beta;
    }

    k++;

    double obj_val = logit_l1B_obj_fun(y, A, m, B, beta, lambda);
    obj_vals.push_back(obj_val);

    if (!is_finite(obj_val))
      throw Rcpp::exception("infinite obj. value");

    obj_diff = std::fabs(obj_val - obj_prev);
    obj_prev = obj_val;

    const double beta_diff = arma::norm(beta - beta_prev, 2);

    printf("\ti=%i (%i internal, v_err=%f): obj_diff=%f, obj_val=%f, beta_diff=%f\n", 
        k, s, v_err, obj_diff, obj_val, beta_diff);

  } while(k < r_max_iters && obj_diff > 1e-7);

  printf("finished in %i iterations (%i internal): obj_diff=%f\n", k, j, obj_diff);

  return List::create(Named("beta")=beta,
                      Named("obj.vals")=obj_vals);
}

//'
//' Proximal Picard-Opial optimization (based on Argryiou) for a 
//' a multinomial logit + l1 composite problem.  
//' Uses a product of sigmoids envelope.
//'
//' @param r_y observations
//' @param r_m counts
//' @param r_A covariate matrix
//' @param r_B penalty constraint matrix
//' @param r_beta0 starting point
//' @param r_lambda penalty weight
//' @param r_kappa Opial constant (between [0,1])
//' @param r_max_iters maximum iterations
//' @param r_max_po_iters maximum Picard-Opial internal iterations
//' @param r_accel enable/disable acceleration (not implemented here)
//' @param r_throw_at_max_po stop execution if internal iterations are reached
//'   before convergence.
//'
// [[Rcpp::export("logit.l1.comp.env.prox")]]
List logit_l1_opial_env_prox_run(NumericVector r_y, 
    NumericVector r_m, NumericMatrix r_A, 
    NumericMatrix r_B, NumericVector r_beta0,
    double r_lambda, double r_kappa,
    int r_max_iters = 100, int r_max_po_iters = 10000, 
    bool r_accel = true, bool r_throw_at_max_po = false) { 

  const arma::vec y = Rcpp::as<arma::vec>(r_y);
  const arma::vec m = Rcpp::as<arma::vec>(r_m);
  const arma::mat A = Rcpp::as<arma::mat>(r_A);
  const arma::mat B = Rcpp::as<arma::mat>(r_B);

  const int N = A.n_rows;
  const int M = A.n_cols;

  // Opial k-average/contraction constant?
  const double kappa = r_kappa; //as<double>(r_kappa);
  // regularization weight/factor
  const double lambda = r_lambda; //as<double>(r_lambda);

  // eigen value bound of contstraint matrix B
  mat U_B,V_B;
  vec s_vals_B;
  arma::svd(U_B, s_vals_B, V_B, B);
  // truncate at small values
  s_vals_B.transform([] (double s) {
        return (fabs(s) < 1e-7) ? 0.0 : s;
      });
  const double omega_B = pow(arma::max(s_vals_B), 2);

  mat U_A,V_A;
  vec s_vals_A;
  arma::svd(U_A, s_vals_A, V_A, A);
  // truncate at small values
  s_vals_A.transform([] (double s) {
        return (fabs(s) < 1e-7) ? 0.0 : s;
      });
  const double omega_A = pow(arma::max(s_vals_A), 2);

  vec s_inv_vals_A(s_vals_A);
  s_inv_vals_A.transform([] (double s) {
        return (fabs(s) < 1e-7) ? 0.0 : 1.0/s;
      });
  mat Ainv_diag(M, N);
  Ainv_diag.diag() = s_inv_vals_A;
  const mat Ainv = V_A * (Ainv_diag * U_A.t());
  
  const int max_iters = r_max_iters; //as<int>(r_max_iters);

  const int iters_fp = r_max_po_iters;
  const bool throw_at_max_po = r_throw_at_max_po;

  printf("N=%i, M=%i, lambda=%f, omega_B=%f, kappa=%f, max_iters=%i, accel=%i\n", 
         N, M, lambda, omega_B, kappa, max_iters, r_accel);

  vec beta = Rcpp::as<arma::vec>(r_beta0);

  std::vector<double> obj_vals;

  int k = 0;
  // running total of internal iterations
  int j = 0;
  vec beta_prev = beta;        
  double obj_prev = logit_l1B_obj_fun(y, A, m, B, beta, lambda);
  obj_vals.push_back(obj_prev);
  double obj_diff = 0.0; 
  const vec eta = arma::sum(diagmat((2.0 * y) - m) * A, 0).t();
  Progress p(0, false);
  do {
    if (Progress::check_abort())
      return R_NilValue;
    vec lambda_Ab = A * beta;
    lambda_Ab.transform([] (double sv) { return logit_lam_fun(sv); });
    const vec D = m % lambda_Ab;
    const mat Q = 2.0 * (A.t() * diagmat(D) * A);

    vec Dinv(D);
    Dinv.transform([] (double s) {
          return (fabs(s) < 1e-7) ? 0.0 : 1.0/s;
        });
    const mat Qinv = 0.5 * Ainv * diagmat(Dinv) * Ainv.t();
    //const mat Qinv = pinv(Q);
    const mat BQinv = B * Qinv;

    // TODO what should we do about the step size?
    const mat BQinvB = BQinv * B.t();
    // TODO don't think we really need to compute the 2-norm
    // from scratch...
    const double omega = //2.0 / pow(arma::norm(BQinvB, 2), 2); 
                       2.0 / arma::norm(BQinvB, 2); 
    printf("\tomega=%f\n", omega);

    // TODO if B^T Q^{-1} B is invertible (i.e. sigma_i > 0)
    // then no need or opial averaging.
    
    // otherwise, omega has to satisfy ||I-omega * B^T Q^{-1} B|| <= 1 
    // for the H operator to be non-expansive.
    // (or |1 - omega * lambda_i(B^T Q^{-1} B)| <= 1, for all i)
    // TODO perform this check in a debug mode?
    //const double norm_check = arma::norm(arma::eye(M, M) - omega * BQinvB, 2);
    //if (fabs(norm_check - 1) > EPS) {
    //  std::stringstream msg;
    //  msg << "fixed point rate, omega, is outside of range!";
    //  msg << "norm_check=" << norm_check;
    //  throw Rcpp::exception(msg.str().c_str());
    //}
    
    vec v = zeros(M);
    vec prev_fp;
    int s = 0;
    double v_err = 0.0; 
    do {
      prev_fp = v;
      vec Bv = B.t() * v;
      vec Av = v - omega * BQinv * Bv + BQinv * eta;
      v = kappa * v + (1.0 - kappa) * (Av - soft_thresh(Av, lambda, omega));
      v_err = norm(prev_fp - v, 2);
      s++;
      if (s >= iters_fp) {
        if (throw_at_max_po)
          throw Rcpp::exception("picard-opial hasn't converged under max iterations");
        printf("reached max internal fixed point iterations! v_err=%f\n", v_err);
        break;
      } 
    } while(v_err > EPS);

    j += s;
                              
    //printf("s=%i, v_diff=%f\\n", s, norm(prev_fp - v, 2));

    beta_prev = beta;
    beta = Qinv * eta - omega * BQinv.t() * v;

    k++;

    double obj_val = logit_l1B_obj_fun(y, A, m, B, beta, lambda);
    obj_vals.push_back(obj_val);

    if (!is_finite(obj_val))
      throw Rcpp::exception("infinite obj. value");

    obj_diff = std::fabs(obj_val - obj_prev);
    obj_prev = obj_val;
    const double beta_diff = arma::norm(beta - beta_prev, 2);

    printf("\ti=%i (%i internal, v_err=%f): obj_diff=%f, obj_val=%f, beta_diff=%f\n", 
        k, s, v_err, obj_diff, obj_val, beta_diff);

  } while(k < max_iters && obj_diff > 1e-9);

  printf("finished in %i iterations (%i internal): obj_diff=%f\n", k, j, obj_diff);

  return List::create(Named("beta")=beta,
                      Named("obj.vals")=obj_vals);
}

