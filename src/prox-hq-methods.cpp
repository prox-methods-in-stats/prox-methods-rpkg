
/**
 * Methods for Half-Quadratic minimization.
 *
 */

#include "prox-methods.h"

/**
 *' Half-quadratic Geman-Reynolds optimization for
 *' a given quadratic, x^T Q x - 2 q^T x + mu + phi(delta),
 *' with penalty phi(delta), where delta = V^T x.
 *' XXX: not finished.
 *'
 */
List hq_gr_run(NumericVector r_y, 
    NumericMatrix r_Q, NumericVector r_q, 
    NumericMatrix r_V, NumericVector r_w, 
    double r_alpha, int r_max_iters) {
    // Function phi,

  const arma::vec y = Rcpp::as<arma::vec>(r_y);
  const arma::vec q = Rcpp::as<arma::vec>(r_q);
  const arma::vec w = Rcpp::as<arma::vec>(r_w);
  const arma::mat Q = Rcpp::as<arma::mat>(r_Q);
  const arma::mat V = Rcpp::as<arma::mat>(r_V);

  const double alpha = r_alpha; // as<double>(r_alpha);
  const int max_iters = r_max_iters; // as<int>(r_max_iters);

  const int N = Q.n_rows;
  const int M = Q.n_cols;

  printf("N=%i, M=%i, max_iters=%i\\n", N, M, max_iters);

  vec x = zeros(M);

  int k = 0;
  vec x_prev = x;
  double obj_prev = l2_dp_obj_val_gr(y, Q, V, w, q, x, 1.0);
  double obj_diff = 0.0; 
  do {
    // TODO: GR...
    // Given x^T Q x - 2 q^T x + mu + phi(delta)
    // for delta = V^T x - w
    // B_GR = 2 Q + V * L * V^T
    // L(x) = diag(dphi(delta)/delta)
    // l_new = dphi(delta)/delta
    // x_new = B_GR_inv * (2*q + V*L*w)
    
    vec delta = V.t() * x - w;
    vec l = delta;
    l.transform([&] (double v) {
        return 1/(alpha*alpha + alpha * std::fabs(v));
        });
    l /= delta;
    mat L = diagmat(l);
    mat VL = V * L;
    mat B_gr = 2 * Q + VL * V.t();
    vec eta = 2 * q + VL * w;
    x = solve(B_gr, eta);

    x_prev = x;

    k++;

    double obj_val = l2_dp_obj_val_gr(y, Q, V, w, q, x, 1.0);
    obj_diff = std::fabs(obj_val - obj_prev);
    obj_prev = obj_val;

  } while(k < max_iters && obj_diff > 1e-7);

  printf("finished in %i iterations: obj_diff=%f\\n", k, obj_diff);

  return List::create(Named("x")=x);
}


