// -*- mode: C++; c-indent-level: 4; c-basic-offset: 4; indent-tabs-mode: nil; -*-
/**
 *
 * Functions for proximal algorithms.
 *
 * From: http://gallery.rcpp.org/articles/passing-cpp-function-pointers/
 * When/if you want to make an R package from this code:
 *  Rcpp.package.skeleton("NewPackage", 
 *    example_code = FALSE, 
 *    cpp_files = c("this.cpp"))
 *
 *  For debugging, remember to remove any optimization flags (e.g. -O3) from
 *  your RqMakeconf & Makevars files (global and local, e.g. ~/.R/Makevars
 *  and /usr/lib/R/etc/Makeconf).
 *  Next, run R with --debugger=gdb.
 *
 **/

#ifndef __PROX_METHODS_H__
#define __PROX_METHODS_H__

#include "RcppArmadillo.h"
#define EPS 1e-7

// via the depends attribute we tell Rcpp to create hooks for
// RcppArmadillo so that the build process will know what to do
//
// [[Rcpp::depends(RcppArmadillo,RcppProgress)]]


using namespace arma; 
using namespace Rcpp;

template<class Matrix>
void print_matrix(Matrix matrix) {
  matrix.print(std::cout);
}
template void print_matrix<arma::mat>(arma::mat matrix);

template<class Vector>
void print_vector(Vector vector) {
  // transpose because they're column vectors
  trans(vector).print(std::cout);
}
template void print_vector<arma::vec>(arma::vec vector);

inline double is_zero(double x) {
  return std::fabs(x) < EPS;
}

inline vec soft_thresh(const vec &beta, double lambda, double omega) {
  vec res(beta.n_elem);
  for (int i = 0; i < beta.n_elem; i++) {
    res(i) = fmax(0, beta(i) - lambda/omega) - fmax(0, -beta(i) - lambda/omega);
  }
  return res;
}

inline vec l2_grad_fun(vec &z, const vec &y, 
                       const mat &X) {
  return X.t() * (X * z - y);
}

inline double l2_l1B_obj_fun(const vec &y, const mat &X, const mat &B, vec &beta, 
                      double sigma, double lambda0) {
  return sum(square(y - X * beta))/(2.0*sigma*sigma) 
    + lambda0 * sum(abs(B * beta))/(sigma*sigma);
}

inline vec l2_env_grad_fun(vec &z, const mat &Q, const vec &q) {
  return Q * z - q;
}

inline double logit_lam_fun(double x) {
  if (is_zero(x))
    return 1.0/8.0;
  return 0.5*(1.0/(1.0+exp(-x)) - 0.5)/x;
}

inline mat svd_solve(const mat&Q, const mat& B) {
  mat U,V;
  vec s;
  arma::svd(U,s,V,Q);
  mat res = V * diagmat(1/s) * trans(U) * B;
  return res;
}

inline mat gen_solve(const mat& Q, const mat& B) {
  mat res;
  if (Q.n_rows > Q.n_cols) {
    // skinny matrix (over-determined)
    res = arma::solve(Q.t(), B.t()).t();  
  } else {
    // fat matrix (under-determined)
    res = svd_solve(Q, B);
  }
  return res;
}

//'
//' Logit + L1 objective function.
//'
//' @param y Observations
//' @param A Covariates/predictor matrix
//' @param m Observation counts.
//' @param B Penalty/constraint matrix.
//' @param beta Parameter matrix.
//' @param lambda Penalty weight.
//'
// [[Rcpp::export("logit.l1.obj")]]
inline double logit_l1B_obj_fun(const vec &y, const mat &A, 
                      const vec &m, const mat &B, vec &beta, 
                      const double lambda) {
  const vec Ab = A * beta; 
  return arma::as_scalar(m.t() * arma::log(1+arma::exp(Ab)) - y.t() * Ab)
    + lambda * arma::sum(arma::abs(B * beta));
}

/**
 *' objective function for L2 loss and double-pareto penalty
 *'
 */
inline double l2_dp_obj_val_gr(const vec &y, const mat &Q, 
                      const mat &V, const vec &w, 
                      const vec &q, const vec &x, 
                      double alpha) {
  return arma::as_scalar(x.t() * Q * x - 2 * q.t() * x + y.t() * y) + 
    arma::sum(arma::abs(V * x - w)/alpha - arma::log(1+arma::abs(V * x - w)/alpha));
}


//'
//' Logit gradient objective function.
//'
//' @param y Observations
//' @param A Covariates/predictor matrix
//' @param m Observation counts.
//' @param beta Parameter matrix.
//'
// [[Rcpp::export("logit.grad")]]
inline vec logit_loss_grad_fun(const vec &y, const mat &A, const vec &m,
    const vec &beta) {
  const vec Ab = A * beta;
  vec plogis = exp(Ab)/(1+exp(Ab));
  vec x_grad = A.t() * (m % plogis - y); 
  return x_grad;
}

//typedef vec (*funcPtr) (const vec& x);

/*
 * Examples of R-cpp function objects
 *
// [[Rcpp::export]]
XPtr<funcPtr> putFunPtrInXPtr(std::string fstr) {
    if (fstr == "fun1")
        return(XPtr<funcPtr>(new funcPtr(&fun1_cpp)));
    else if (fstr == "fun2")
        return(XPtr<funcPtr>(new funcPtr(&fun2_cpp)));
    else
        return XPtr<funcPtr>(R_NilValue); // runtime error as NULL no XPtr
}

// [[Rcpp::export]]
vec callViaString(const vec x, std::string funname) {
    XPtr<funcPtr> xpfun = putFunPtrInXPtr(funname);
    funcPtr fun = *xpfun;
    vec y = fun(x);
    return (y);
}

// [[Rcpp::export]]
vec callViaXPtr(const vec x, SEXP xpsexp) {
    XPtr<funcPtr> xpfun(xpsexp);
    funcPtr fun = *xpfun;
    vec y = fun(x);
    return (y);
}
*/

#endif

